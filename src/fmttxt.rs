use std::env;
use std::fs::File;
use std::io::BufReader;
use std::io::Read;

struct Formatter {
    string: String,
    x: usize,
    y: usize,
    page: usize,
    maxx: usize,
    maxy: usize,
}

impl Formatter {
    pub fn new(s: String) -> Self {
        Self {
            string: s,
            x: 0,
            y: 0,
            page: 1,
            maxx: 70,
            maxy: 40,
        }
    }

    pub fn format(&mut self) {
        for (i, c) in self.string.chars().enumerate() {
            // new page
            if self.x == 0 && self.y == 0 {
                println!("\n\n");
            }

            // new line
            if c == '\n' {
                print!("{}", c);
                self.x = 0;
                self.y += 1;
                continue;
            }

            // start of a line
            if self.x == 0 {
                print!("    ");
                self.x += 5;
            }

            // end of line
            if self.x >= (self.maxx - 5) {
                println!("{}", c);
                self.y += 1;
                self.x = 0;
            } else {
                print!("{}", c);
                self.x += 1;
            }

            // end of page
            if self.y > self.maxy {
                println!("\n\n     {}\n\n", self.page);
                self.y = 0;
                self.x = 0;
                self.page += 1;
            }
        }

        // finish off the remaining part of the page
        for i in self.y..(self.maxy - 1) {
            println!();
        }
        println!("     {}\n\n", self.page);
    }
}

fn main() {
    let args: Vec<String> = env::args().collect::<Vec<String>>();

    if args.len() < 2 {
        eprintln!("Usage: fmttxt [FILE]");
        return;
    }

    let file = File::open(&args[1]).expect("Failed to open file");
    let mut reader = BufReader::new(file);
    let mut content = String::new();

    reader.read_to_string(&mut content).expect("Failed to read to string");

    let mut fmt = Formatter::new(content);
    fmt.format();
}
